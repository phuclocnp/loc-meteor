import { Meteor } from 'meteor/meteor';
import { Players } from '../imports/api/players';

Meteor.startup(() => {
  Meteor.publish('players', () => {
    return Players.find({});
  })
});
