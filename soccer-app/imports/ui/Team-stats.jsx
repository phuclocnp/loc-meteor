import React, { Component } from 'react'
import { Radar } from 'react-chartjs-2'
import Divider from 'material-ui/Divider'

export default class TeamStats extends Component {
  render() {
    const players = this.props.players;
    const numPlayers = players.length;

    const ballManipulation = Math.round((players.reduce((ballManipulation, player) => {
      return ballManipulation + player.ballManipulation
    }, 0) / (3 * numPlayers)) * 100);

    const kicking = Math.round((players.reduce((kicking, player) => {
      return kicking + player.kickingAbilities
    }, 0) / (3 * numPlayers)) * 100);

    const passing = Math.round((players.reduce((passing, player) => {
      return passing + player.passingAbilities
    }, 0) / (3 * numPlayers)) * 100);

    const duel = Math.round((players.reduce((duel, player) => {
      return duel + player.duelTackling
    }, 0) / (3 * numPlayers)) * 100);

    const field = Math.round((players.reduce((field, player) => {
      return field + player.fieldCoverage
    }, 0) / (3 * numPlayers)) * 100);

    const blocking = Math.round((players.reduce((blocking, player) => {
      return blocking + player.blockingAbilities
    }, 0) / (3 * numPlayers)) * 100);

    const gameStrategy = Math.round((players.reduce((gameStrategy, player) => {
      return gameStrategy + player.gameStrategy
    }, 0) / (3 * numPlayers)) * 100);

    const risks = Math.round((players.reduce((risks, player) => {
      return risks + player.playmakingRisks
    }, 0) / (3 * numPlayers)) * 100);

    const data = {
      labels: ['Ball Manipulation', 'Kicking', 'Passing', 'Duel/Tackling', 'Field Coverage', 'Blocking', 'Strategy' ,'Risks'],
      datasets: [
        {
          label: 'In % of max possible',
          backgroundColor: 'rgba(143,202,249,0.2)',
          borderColor: 'rgba(12,71,161,1)',
          pointBackgroundColor: 'rgba(12,71,161,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(12,71,161,1)',
          data: [ballManipulation, kicking, passing, duel, field, blocking, gameStrategy, risks]
        }
      ]
    };

    const defense = Math.round((duel + gameStrategy + field + blocking + risks) / 5);
    const offense = Math.round((kicking + passing + gameStrategy + ballManipulation) / 4);
    const total = Math.round((kicking + passing + gameStrategy + ballManipulation + blocking + duel + risks + field) / 8);
    
    return (
      <div>
        <h2>Team stats</h2>
        <div className="row">
          <div className="col s12 m5">
            <Radar data={data}
              width={500}
              height={500}
              option={{
                maintainAspectRatio: false
              }}
            />
          </div>
        </div>
        <div className="col s12 m5">
          <h4>Scores in % of max possible</h4>
          <Divider />
            <h4>Team's offense: {offense}%</h4>
            <h4>Team;s defense: {defense}%</h4>
            <h4>Team's total: {total}%</h4>
          <Divider />
          <h4>Number of players: {numPlayers}</h4>
        </div>
      </div>
    )
  }
}