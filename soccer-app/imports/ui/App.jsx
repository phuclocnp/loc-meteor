import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisesButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';
import { List } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import { createContainer } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';

import { Players } from '../api/players';

import TeamList from './Team-list';
import TeamStats from './Team-stats';
import Player from './Player';
import AccountsWrapper from './AccountsWrapper';
import Edit from './EditPlayer';

const tempPlayer = {
  name: 'Temp player',
  team: 'Loc',
  ballManipulation: 2,
  kickingAbilities: 3,
  passingAbilities: 2,
  duelTackling: 5,
  fieldCoverage: 4,
  blockingAbilities: 3,
  gameStrategy: 5,
  playmakingRisks: 4,
  notes: 'This player is only temporary',
  createdAt: new Date(),
  owner: Meteor.userId()
};

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPlayer: tempPlayer,
      showEditPlayer: false
    };
    this.updateCurrentPlayer = this.updateCurrentPlayer.bind(this);
    this.showEditForm = this.showEditForm.bind(this);
    this.showTeamStats = this.showTeamStats.bind(this);
  }

  renderPlayers() {
    return this.props.players.map(player => (
      <TeamList key={player._id} player={player} updateCurrentPlayer={this.updateCurrentPlayer} />
    ));
  }

  updateCurrentPlayer(player) {
    this.setState({ currentPlayer: player });
  }

  showEditForm() {
    this.setState({ showEditPlayer: true })
  }

  showTeamStats() {
    this.setState({ showEditPlayer: false })
  }

  showForm() {
    if (this.state.showEditPlayer === true) {
      return ( 
        <Edit currentPlayer={this.state.currentPlayer}
          showTeamStats={this.showTeamStats} />
      )
    } else {
      return ( 
        <TeamStats players={this.props.players} />
      )
    }
  }

  render() {
    return (
      <MuiThemeProvider>
        <div className="container">
          <AppBar
            title="Soccer Application"
            iconClassNameRight="muidocs-icon-navigation-expand-more"
            showMenuIconButton={true}
            style={{backgroundColor: '#8277BD'}}
          >
            <AccountsWrapper />
          </AppBar>
          <div className="row">
            <div className="col s12 m7">
              <Player player={this.state.currentPlayer} showEditForm={this.showEditForm} />
            </div>
            <div className="col s12 m5">
              <h2>Team List</h2>
              <Link to="/new" className="waves-effect waves-light btn light-blue darken-3">
                Add player 
              </Link>
              <Divider />
              <List>{this.renderPlayers()}</List>
              <Divider />
            </div>
          </div>
          <div className="row">
            <div className="col s12">
              <br />
              <Divider />
              {this.showForm()}
              <Divider />
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  players: PropTypes.array.isRequired
};

export default createContainer(() => {
  Meteor.subscribe('players');
  const user = Meteor.userId();
  console.log(user);
  return {
    players: Players.find({ owner: user }, { sort: { name: 1 } }).fetch()
  };
}, App);
